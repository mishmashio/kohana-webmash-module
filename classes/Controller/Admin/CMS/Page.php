<?php

class Controller_Admin_CMS_Page extends Controller_Admin {

    public function action_list() {
        $errors = FALSE;
        $pages = ORM::factory("Page")->find_all();
        $this->template->content = View::factory('admin/cms/page/list', array(
                    'errors' => $errors,
                    'pages' => $pages
        ));
    }

    public function action_new() {
        $errors = FALSE;
        if ($this->request->method() == Request::POST) {
            $check = Helpers::checkUnique("Page", 'title',$this->request->post("title"));
            if (!$check) {
                $page = ORM::factory("Page");
                $page->title = $this->request->post("title");
                $page->language = $this->request->post("language");
                $page->save();
                $this->redirect("/page/list");
            } else {
                Flash::error("this page already exists");
            }
        }
        $this->template->content = View::factory('admin/cms/page/page_new', array(
                    'errors' => $errors,
        ));
    }

    public function action_index() {
        $errors = FALSE;
        $page = ORM::factory("Page")->where("guid", "=", $this->request->param("guid"))->find();
       
        if (!$page->loaded()) {
            Flash::error(__("NO such menu!"));
            $this->redirect("/");
        }
        if ($this->request->method() == Request::POST) {

            $check = Helpers::checkUnique("Page", 'title',$this->request->post("title"));
            if (!$check) {
                $page->title = $this->request->post("title");
                
            } elseif (( $page->title == $this->request->post("title"))) {
//                //do nothing
            } else {
                Flash::error("this page already exists");
            }

            $page->language = $this->request->post("language");
            $page->save();
            $page->reload();
            $this->redirect("/page/list");
        }
        $this->template->content = View::factory('admin/cms/page/page', array(
                    'errors' => $errors,
                    'page' => $page,
        ));
    }

}
