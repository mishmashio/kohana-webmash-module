<?php

class Controller_Admin_CMS_Menu extends Controller_Admin {

    public function action_list() {
        $errors = FALSE;
        $menus = ORM::factory("Menu")->find_all();
        $this->template->content = View::factory('admin/cms/menu/list', array(
                    'errors' => $errors,
                    'menus' => $menus
        ));
    }

    public function action_new() {
        $errors = FALSE;
        if ($this->request->method() == Request::POST) {
            $check = Helpers::checkUnique("Menu", 'name',$this->request->post("name"));
            if (!$check) {
                $menu = ORM::factory("Menu");
                $menu->name = $this->request->post("name");
                $menu->save();
                $this->redirect("/menu/list");
            } else {
                Flash::error("this menu already exists");
            }
        }
        $this->template->content = View::factory('admin/cms/menu/menu_new', array(
                    'errors' => $errors,
        ));
    }

    public function action_index() {
        $errors = FALSE;
        $menu = ORM::factory("Menu")->where("guid", "=", $this->request->param("guid"))->find();
       
        if (!$menu->loaded()) {
            Flash::error(__("NO such menu!"));
            $this->redirect("/");
        }
        if ($this->request->method() == Request::POST) {

            $check = Helpers::checkUnique("Menu", 'name',$this->request->post("name"));
            if (!$check) {
                $menu->name = $this->request->post("name");
                
            } elseif (( $menu->name == $this->request->post("name"))) {
//                //do nothing
            } else {
                Flash::error("this name already exists");
            }
            $menu->save();
            $menu->reload();
            $this->redirect("/menu/list");
        }
        $this->template->content = View::factory('admin/cms/menu/menu', array(
                    'errors' => $errors,
                    'menu' => $menu,
        ));
    }
    
    public function item_new() {
        $errors = FALSE;
        if ($this->request->method() == Request::POST) {
            $check = Helpers::checkUnique("Menu_Item", 'title',$this->request->post("title"));
            if (!$check) {
                $item = ORM::factory("Menu_Item");
                $item->title = $this->request->post("title");
                $item->ord = $this->request->post("order");
                $item->href = $this->request->post("link");
                $item->language = $this->request->post("language");
                $item->save();
                $this->redirect("/menu/item/list");
            } else {
                Flash::error("this menu item already exists");
            }
        }
        $this->template->content = View::factory('admin/cms/menu/item_new', array(
                    'errors' => $errors,
        ));
    }

}
