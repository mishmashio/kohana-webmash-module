<?php

class Controller_Admin_CMS_Menu_Item extends Controller_Admin {

    public function action_list() {
        $errors = FALSE;
        $items = ORM::factory("Menu_Item")->find_all();
        $this->template->content = View::factory('admin/cms/menu/item/list', array(
                    'errors' => $errors,
                    'items' => $items
        ));
    }

    public function action_index() {
        $errors = FALSE;
        $item = ORM::factory("Menu_Item")->where("guid", "=", $this->request->param("guid"))->find();
       
        if (!$item->loaded()) {
            Flash::error(__("NO such menu item!"));
            $this->redirect("/");
        }
        if ($this->request->method() == Request::POST) {

            $check = Helpers::checkUnique("Menu_Item", 'title',$this->request->post("title"));
            if (!$check) {
                $item->title = $this->request->post("title");
                
            } elseif (( $item->title == $this->request->post("title"))) {
//                //do nothing
            } else {
                Flash::error("this title already exists");
            }
            $item->save();
            $item->reload();
            $this->redirect("/menu/item/list");
        }
        $this->template->content = View::factory('admin/cms/menu/item/item', array(
                    'errors' => $errors,
                    'item' => $item,
        ));
    }

}
