<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_Workinghours extends ORM {

    protected $_table_name = 'workinghours';
    protected $_belongs_to = array(
    'merchant' => array(
        'model' => 'Merchant',
        'foreign_key' => 'merchant_id'
        )
    );
    
    
    
//    protected $_rules = array(
//        'guid' => array(
//            'not_empty' => NULL,
//            'min_length' => array(36),
//            'max_length' => array(36),
//        //'regex'      => array('/^[-\pL\pN_.]++$/uD'),
//        ),
//        'email' => array(
//            'not_empty' => NULL,
//            'min_length' => array(8),
//            'max_length' => array(255),
//        //'regex'      => array('/^[-\pL\pN_.]++$/uD'),
//        ),
//        'names' => array(
//            'not_empty' => NULL,
//            'min_length' => array(5),
//            'max_length' => array(255),
////            'regex'      => array('/^[-\pL\pN_.]++$/uD'),
//        )
//    );

    public function get_url() {
//		if ($this->user_object->username != "") {
//			return "/".$this->user_object->username;
//		}
        return "/admin/account/" . $this->guid . "/";
    }

//    public function to_ajax() {
//        $data = array(
//            "guid" => $this->guid,
//            "names" => $this->names,
//            "name" => $this->names,
//            "created" => $this->created_ts,
//            "modified" => $this->modified_ts,
//            "profile_image" => $this->get_url() . "/profileimage",
//            "profile_url" => $this->get_url()
//        );
//        return $data;
//    }

}